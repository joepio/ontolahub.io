# Webpage & blog for Ontola
Check it out [here](https://onto.la).
This project runs on Jekyll and is hosted by Github Pages.

## Run
* `git clone git@github.com:ontola/ontola.github.io.git`
* Install [GitHub Pages](https://help.github.com/articles/setting-up-your-github-pages-site-locally-with-jekyll/) or [Jekyll](https://jekyllrb.com/docs/installation/)
* `jekyll serve`
* Visit [`localhost:4000`](http://localhost:4000)
